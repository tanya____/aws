#------------------------------------------------------------------------------------ 
# Terraform CHANGELOG : https://github.com/hashicorp/terraform/blob/main/CHANGELOG.md
# Provider AWS CHANGELOG : https://github.com/hashicorp/terraform-provider-aws/blob/main/CHANGELOG.md
#------------------------------------------------------------------------------------

#
# Initialisation du backend distant
# https://developer.hashicorp.com/terraform/language/settings/backends/s3
#
terraform {
  backend "s3" {
    region         = "us-east-1"
    bucket         = "enzobucketepita"
    key            = "epita/bounac_m/infra.tfstate"
    dynamodb_table = "enzotableepita"
  }
}
