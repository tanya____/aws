terraform {
  required_version = "1.4.2"
  required_providers {
    aws = "4.67.0"
  }
}

provider "aws" {
  region = "us-east-1"
}
